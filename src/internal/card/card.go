package card

import "time"

type Day struct {
	Complete bool   `json:"complete"`
	Success  bool   `json:"success"`
	Excuse   string `json:"excuse"`
}

type Card struct {
	Name     string    `json:"name"`
	Title    string    `json:"title"`
	Why      string    `json:"why"`
	Start    time.Time `json:"start"`
	Days     []Day     `json:"days"`
	Archived bool      `json:"archived"`
}

func New() Card {
	card := Card{}
	card.Days = make([]Day, 49)
	return card
}

func (c *Card) SetDay(i int, success bool, excuse string) {
	day := Day{
		Complete: true,
		Success:  success,
	}
	if success == false {
		day.Excuse = excuse
	}

	c.Days[i] = day
}
