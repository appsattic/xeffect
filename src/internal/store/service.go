package store

import "internal/card"

type StoreService interface {
	SelCards() ([]*card.Card, error)
	GetCard(id string) (*card.Card, error)
	PutCard(c card.Card) error
	GetCardArchive(id string) (*card.Card, error)
	PutCardArchive(c card.Card) error
}
