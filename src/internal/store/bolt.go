package store

import (
	"time"

	"github.com/boltdb/bolt"
	"github.com/chilts/rod"

	"internal/card"
)

var cardsBucket = "cards"
var archivesBucket = "archives"

type BoltStore struct {
	filename string
	db       *bolt.DB
}

func now() time.Time {
	return time.Now().UTC()
}

func NewBoltStore(filename string) *BoltStore {
	return &BoltStore{
		filename: filename,
	}
}

func (b *BoltStore) Open() error {
	db, err := bolt.Open(b.filename, 0600, &bolt.Options{Timeout: 1 * time.Second})
	b.db = db
	return err
}

func (b *BoltStore) Close() error {
	return b.db.Close()
}

func (b *BoltStore) SelCards() ([]*card.Card, error) {
	cards := make([]*card.Card, 0)

	err := b.db.View(func(tx *bolt.Tx) error {
		return rod.SelAll(tx, cardsBucket, func() interface{} {
			return card.Card{}
		}, func(v interface{}) {
			c, _ := v.(card.Card)
			cards = append(cards, &c)
		})
	})

	return cards, err
}

func (b *BoltStore) GetCard(name string) (*card.Card, error) {
	card := card.Card{}

	err := b.db.View(func(tx *bolt.Tx) error {
		return rod.GetJson(tx, cardsBucket, name, &card)
	})

	if err != nil {
		return nil, err
	}
	return &card, nil
}

func (b *BoltStore) PutCard(card card.Card) error {
	err := b.db.Update(func(tx *bolt.Tx) error {
		return rod.PutJson(tx, cardsBucket, card.Name, &card)
	})
	return err
}

func (b *BoltStore) GetCardArchive(name string) (*card.Card, error) {
	card := card.Card{}

	err := b.db.View(func(tx *bolt.Tx) error {
		return rod.GetJson(tx, archivesBucket, name, &card)
	})

	if err != nil {
		return nil, err
	}
	return &card, nil
}

func (b *BoltStore) PutCardArchive(card card.Card) error {
	err := b.db.Update(func(tx *bolt.Tx) error {
		return rod.PutJson(tx, archivesBucket, card.Name, &card)
	})
	return err
}
