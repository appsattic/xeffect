package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"github.com/chilts/sandpeople"

	"internal/card"
	"internal/store"
)

func homeHandler(tmpl *template.Template) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// get the user from context (and their cards too)
		user := sandpeople.GetUser(r)
		var cards []*card.Card
		if user == nil {
			// all ok, either we have a user, or we don't
		} else {
			// we have a user, therefore, we should have some cards
			cards = getCardsFromRequest(r)
		}

		data := struct {
			Title string
			User  *sandpeople.User
			Cards []*card.Card
		}{
			"Home",
			user,
			cards,
		}

		render(w, tmpl, "index.html", data)
	}
}

func newCardFormHandler(tmpl *template.Template) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		data := struct {
			Title  string
			User   *sandpeople.User
			Form   map[string]string
			Errors map[string]string
		}{
			"New Card",
			sandpeople.GetUser(r),
			nil,
			nil,
		}

		render(w, tmpl, "card-new.html", data)
	}
}

func newCardPostHandler(tmpl *template.Template, s store.StoreService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// parse the form
		r.ParseForm()

		form := make(map[string]string)
		form["Title"] = r.FormValue("Title")
		form["Why"] = r.FormValue("Why")

		errors := make(map[string]string)

		if form["Title"] == "" {
			errors["Title"] = "Provide a title"
		}

		if len(errors) > 0 {
			// get the user from context (and their cards too)
			user := sandpeople.GetUser(r)

			data := struct {
				Title  string
				User   *sandpeople.User
				Form   map[string]string
				Errors map[string]string
			}{
				"New Card",
				user,
				form,
				errors,
			}

			render(w, tmpl, "card-new.html", data)
			return
		}

		// let's insert this new card
		c := card.New()
		c.Name = randId(12)
		c.Title = form["Title"]
		c.Why = form["Why"]
		c.Start = time.Now()
		c.Archived = false

		err := s.PutCard(c)
		if err != nil {
			internalServerError(w, err)
			return
		}

		// redirect to the new card
		redirectTemporarily(w, r, "/card/"+c.Name)
	}
}

func boardsIndexHandler(tmpl *template.Template, s store.StoreService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// get the user from context
		user := sandpeople.GetUser(r)
		if user == nil {
			// Program Error: shouldn't be empty since we have sandpeople.RequireUser("/") in middleware before this
		}

		data := struct {
			Title string
			User  *sandpeople.User
		}{
			"Board Index",
			user,
		}

		render(w, tmpl, "boards.html", data)
	}
}

func cardHandler(tmpl *template.Template) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// get things from the context
		c := getCardFromRequest(r)
		user := sandpeople.GetUser(r)

		fmt.Printf("user=%#v\n", user)
		fmt.Printf("card=%#v\n", c)

		data := struct {
			Title string
			User  *sandpeople.User
			Card  *card.Card
		}{
			c.Title,
			user,
			c,
		}

		render(w, tmpl, "card-:cardName.html", data)
	}
}
