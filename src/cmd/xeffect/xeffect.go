package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/chilts/sandpeople"
	"github.com/gomiddleware/mux"
	"github.com/gomiddleware/slash"

	"internal/card"
	"internal/store"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func render(w http.ResponseWriter, tmpl *template.Template, tmplName string, data interface{}) {
	buf := &bytes.Buffer{}
	err := tmpl.ExecuteTemplate(buf, tmplName, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	buf.WriteTo(w)
}

func main() {
	c := card.New()
	fmt.Printf("c=%#v\n", c)

	// setup
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("Specify a port to listen on in the environment variable 'PORT'")
	}

	// load up all templates
	funcMap := template.FuncMap{
		"divide": divide,
		"rem":    rem,
		"mod":    mod,
	}
	tmpl, err := template.New("").Funcs(funcMap).ParseGlob("./templates/*.html")
	if err != nil {
		log.Fatal(err)
	}

	// create/open/connect to a store
	boltStore := store.NewBoltStore("xeffect.db")
	errOpen := boltStore.Open()
	check(errOpen)
	defer boltStore.Close()

	// router
	m := mux.New()

	m.All("/s", fileServer("static"))
	m.Get("/favicon.ico", serveFile("./static/favicon.ico"))
	m.Get("/robots.txt", serveFile("./static/robots.txt"))

	// call MakeUser, and the endpoints can call GetUser() if needed. Also MockUser if the env says so.
	if os.Getenv("SANDSTORM") == "1" || os.Getenv("SANDSTORM") == "true" || os.Getenv("SANDSTORM") == "yes" {
		// see if we should mock a user
		if os.Getenv("MOCK") == "1" {
			user := sandpeople.User{
				ID:          "pG2n1bOZTdG2",
				Name:        "Bob Jones",
				Permissions: []string{"admin"},
				Pronoun:     "it",
				Handle:      "bobby",
				Avatar:      "https://placekitten.com/g/256/256",
			}
			m.Use("/", sandpeople.MockUser(user))
		}
		m.Use("/", sandpeople.MakeUser)
	}

	// this can also be your daily review
	m.Get("/", LoadAllCards(boltStore), homeHandler(tmpl))

	// new card
	m.Get("/card/new", newCardFormHandler(tmpl))
	m.Post("/card/new", newCardPostHandler(tmpl, boltStore))

	// // for all routes from "/b/" downwards, make sure the user is authenticated
	m.Get("/card", slash.Add)
	// m.Get("/card/", loadCards, cardsHandler)
	m.Get("/card/:cardName", loadCard(boltStore), cardHandler(tmpl))

	// // archive
	// m.Get("/archive", slash.Add)
	// m.Get("/archive/", loadArchivedCards, archivedCardsHandler)
	// m.Get("/archive/:cardName", loadArchivedCard, archiveCardHandler(tmpl, boltStore))

	// check that all routes were added correctly
	check(m.Err)

	// server
	log.Printf("Starting server, listening on port %s\n", port)
	errServer := http.ListenAndServe(":"+port, m)
	check(errServer)
}
