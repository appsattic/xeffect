package main

import "net/http"

func serveFile(filename string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	}
}

func fileServer(dirname string) http.Handler {
	return http.FileServer(http.Dir(dirname))
}

func internalServerError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func redirectTemporarily(w http.ResponseWriter, r *http.Request, path string) {
	http.Redirect(w, r, path, http.StatusFound)
}

func notFound(w http.ResponseWriter, r *http.Request) {
	http.NotFound(w, r)
}
