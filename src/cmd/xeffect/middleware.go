package main

import (
	"context"
	"net/http"

	"github.com/gomiddleware/mux"

	"internal/card"
	"internal/store"
)

type key int

const cardKey key = 8588
const cardsKey key = 1827

func LoadAllCards(s store.StoreService) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			cards, err := s.SelCards()
			if err != nil {
				internalServerError(w, err)
				return
			}

			// put the cards into the context
			ctx := context.WithValue(r.Context(), cardsKey, cards)

			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(fn)
	}
}

func loadCard(s store.StoreService) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// get the cardName from the route
			vals := mux.Vals(r)
			cardName := vals["cardName"]

			card, err := s.GetCard(cardName)
			if err != nil {
				internalServerError(w, err)
				return
			}

			if card.Name == "" {
				notFound(w, r)
				return
			}

			// put the card into the context
			ctx := context.WithValue(r.Context(), cardKey, card)

			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(fn)
	}
}

func getCardsFromRequest(r *http.Request) []*card.Card {
	return r.Context().Value(cardsKey).([]*card.Card)
}

func getCardFromRequest(r *http.Request) *card.Card {
	return r.Context().Value(cardKey).(*card.Card)
}
