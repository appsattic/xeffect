all:
	echo 'Provide a target: xeffect clean'

vendor:
	gb vendor fetch github.com/boltdb/bolt
	gb vendor fetch github.com/gomiddleware/mux
	gb vendor fetch github.com/gomiddleware/slash
	gb vendor fetch github.com/chilts/rod

fmt:
	find src/ -name '*.go' -exec go fmt {} ';'

build: fmt
	gb build all

xeffect: build
	SANDSTORM=1 MOCK=1 ./bin/xeffect

test:
	gb test -v

clean:
	rm -rf bin/ pkg/

.PHONY: xeffect
