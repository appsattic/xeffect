var months = [
  "Jan", "Feb", "Mar", "Apr",
  "May", "Jun", "Jul", "Aug",
  "Sep", "Oct", "Nov", "Dec"
];

var today = new Date()
var day = today.getDate();
var month = today.getMonth();
var year = today.getFullYear();

var dates = document.getElementsByClassName('js-date');
for(var i = 0; i < dates.length; i++) {
  dates[i].innerText = '' + day + '-' + months[month] + '-' + year
}
